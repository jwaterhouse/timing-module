This is a system to control and manipulate an ADC module (implemented on an Arduino board) via a web server (implemented on a Raspberry Pi).

Prerequisites:

* wiringPi - http://wiringpi.com/download-and-install/

Compiling the server:

* Install prerequisites and type 'make'.
* Type 'sudo server config.ini' to run.
* Open a browser and navigate to the RPi's IP address (default port: 80)

Compiling the ADC:

* Using inotool just use 'ino build', or load the sketch into the Arduino IDE and compile normally.