#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <libconfig.h>
#include "mongoose.h"
#include "ini.h"
#include "cJSON.h"

// Convenience functions
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

int serial_fd = 0;

// Lock key for when communicating with the ADC
#define SERIAL_LOCK_KEY 0

// Serial command codes
enum SerialCommandCodes
{
  SERIAL_COMMAND_UPDATE = 1,
  SERIAL_COMMAND_PING = 2
};

// Serial return codes
enum SerialReturnCodes
{
  SERIAL_RETURN_TIMEOUT = -1,
  SERIAL_RETURN_OKAY = 1,
  SERIAL_RETURN_ERROR_UNKNOWN_COMMAND = 2,
  SERIAL_RETURN_ERROR_INVALID_DATA = 3,
  SERIAL_RETURN_ERROR = 4
};

// Structure to store server configuration data
typedef struct ServerConfigurationStruct
{
  // Server Settings
  char *document_root;
  char *listening_port;

  // GPIO Settings
  int gpio_input_signal_pin;
  int gpio_output_signal_pin;

  // ADC Settings
  char *adc_serial_port;
  int adc_baud_rate;
} ServerConfiguration;

ServerConfiguration server_config;

// Structure to store ADC configuration data
typedef struct ADCConfigurationStruct
{
  // Input/Output signal and LED pins
  int16_t input_signal_pin;
  int16_t output_signal_pin;
  int16_t input_led_pin;
  int16_t output_led_pin;

  // Arc offset and length are stored as a multiple of tenths of
  // a degree. For example, a value of 123 is 12.3 degrees.

  // Number of degrees to offset the input signal by
  int16_t arc_offset;

  // Number of degrees to to lengthen the input signal
  int16_t arc_length;

  // Number of detector slugs/driver magnets/coils in the system
  int16_t n;
} ADCConfiguration;

ADCConfiguration adc_config;

// Calculate the difference between two times, taking into account overflow
// Assumes that time1 is older or equal to time2, and that time2 has not
// "elapsed" time1
static unsigned int
time_diff(unsigned int time1, unsigned int time2)
{
  if (time1 > time2)
    return 0xFFFFFFFFUL - time1 + time2;
  else
    return time2 - time1;
}

// Parses the .ini configuration file
static int
on_ini_handler(void* user, const char* section, const char* name,
              const char* value)
{
  if (strcmp(name, "document_root") == 0)
    server_config.document_root = strdup(value);
  else if (strcmp(name, "listening_port") == 0)
    server_config.listening_port = strdup(value);
  else if (strcmp(name, "gpio_input_signal_pin") == 0)
    server_config.gpio_input_signal_pin = atoi(value);
  else if (strcmp(name, "gpio_output_signal_pin") == 0)
    server_config.gpio_output_signal_pin = atoi(value);
  else if (strcmp(name, "adc_serial_port") == 0)
    server_config.adc_serial_port = strdup(value);
  else if (strcmp(name, "adc_baud_rate") == 0)
    server_config.adc_baud_rate = atoi(value);
  else if (strcmp(name, "adc_config_input_signal_pin") == 0)
    adc_config.input_signal_pin = atoi(value);
  else if (strcmp(name, "adc_config_output_signal_pin") == 0)
    adc_config.output_signal_pin = atoi(value);
  else if (strcmp(name, "adc_config_input_led_pin") == 0)
    adc_config.input_led_pin= atoi(value);
  else if (strcmp(name, "adc_config_output_led_pin") == 0)
    adc_config.output_led_pin = atoi(value);
  else if (strcmp(name, "adc_config_n") == 0)
    adc_config.n = atoi(value);
  else if (strcmp(name, "adc_config_arc_offset") == 0)
    adc_config.arc_offset = atoi(value);
  else if (strcmp(name, "adc_config_arc_length") == 0)
    adc_config.arc_length = atoi(value);
  else
    return 0;
  return 1;
}

// Retrieves the pulse period on the input pin, and waits no longer
// than the given timeout. If timeout is hit, returns UINT_MAX.
static unsigned int
get_pulse_period(const unsigned int timeout)
{
  unsigned int time1, time2;
  char command[50];
  
  // Initialise the GPIO pin
  sprintf(command, "gpio edge %d falling", server_config.gpio_input_signal_pin);
  if (system(command) == -1)
  {
    printf("*** WARNING *** Could not initialise the input signal pin.\n");
    return UINT_MAX;
  }
  
  // Wait for the first falling edge and record the time
  if (waitForInterrupt(server_config.gpio_input_signal_pin, timeout / 2) <= 0)
    return UINT_MAX;
  time1 = micros();

  // Wait for the second falling edge and record the time
  if (waitForInterrupt(server_config.gpio_input_signal_pin, timeout / 2) <= 0)
    return UINT_MAX;
  time2 = micros();

  // Return the difference between the two times
  return time_diff(time1, time2);
}

static int
setup_serial()
{
  piLock(SERIAL_LOCK_KEY);

  // Close any existing serial connection
  if (serial_fd != 0)
    serialClose(serial_fd);

  // Connect Serial to ADC
  serial_fd = serialOpen(server_config.adc_serial_port, server_config.adc_baud_rate);

  if (serial_fd >= 0)
  {
    printf("Serial connection opened successfully on port %s at %d baud\n", server_config.adc_serial_port, server_config.adc_baud_rate);
    delay(2000);
    serialFlush(serial_fd);
  }
  else
  {
    printf("*** WARNING *** Serial connection failed on port %s at %d baud\n", server_config.adc_serial_port, server_config.adc_baud_rate);
  }

  piUnlock(SERIAL_LOCK_KEY);

  return serial_fd >= 0;
}

static int
send_data_to_adc(char command_code, char *data, unsigned int data_length)
{
  int i, return_code;
  char packet[data_length + 2];

  // Set the header with the command code byte and length of the data
  packet[0] = command_code;
  packet[1] = data_length;

  // Copy the data array into the rest of the data'
  if (data != NULL && data_length)
    memcpy(packet + 2, data, data_length);

  // Lock the actual transmission to prevent simultaneous communication
  piLock(SERIAL_LOCK_KEY);

  // Clear any pending data
  serialFlush(serial_fd);
  
  // Transmit the packet
  for (i = 0; i < data_length + 2; ++i)
    serialPutchar(serial_fd, packet[i]);

  // Get the return code
  return_code = serialGetchar(serial_fd);

  // Prevent too high a frequency of transmission
  delay(500);

  // Release the lock
  piUnlock(SERIAL_LOCK_KEY);

  printf("\tSerial command %d returned with code %d\n", command_code, return_code);
  
  return return_code;
}

// Configure the ADC connection
static void
connect_adc(struct mg_connection *conn)
{
  int return_code;
  char port[100], baud[100], path[PATH_MAX + 1];
  cJSON *root, *ports;
  char *json;
  DIR *dp;
  struct dirent *ep;

  // Handle parameters
  if (mg_get_var(conn, "port", port, sizeof(port)) > 0)
  {
    free(server_config.adc_serial_port);
    server_config.adc_serial_port = strdup(port);
  }

  if (mg_get_var(conn, "baud", baud, sizeof(baud)) > 0)
    server_config.adc_baud_rate = atoi(baud);

  // Setup the serial connection
  return_code = setup_serial();

  // Build the reply JSON string
  root = cJSON_CreateObject();
  cJSON_AddStringToObject(root, "port", server_config.adc_serial_port);
  cJSON_AddNumberToObject(root, "baud", server_config.adc_baud_rate);

  // Add available ports
  ports = cJSON_CreateArray();
  cJSON_AddItemToObject(root, "ports", ports);
  dp = opendir("/dev/");
  memcpy(path, "/dev/", 5);
  if (dp)
  {
    while ((ep = readdir(dp)))
    {
      if (strncmp(ep->d_name, "ttyUSB", 6) == 0)
      {
        memcpy(path + 5, ep->d_name, strlen(ep->d_name));
        path[5 + strlen(ep->d_name) + 1] = '\0';
        cJSON_AddItemToArray(ports, cJSON_CreateString(ep->d_name));
      }
    }
    closedir(dp);
  }


  if (return_code)
  {
    cJSON_AddTrueToObject(root, "success");
  }
  else
  {
    cJSON_AddFalseToObject(root, "success");
    cJSON_AddStringToObject(root, "reason", "Serial connection failed.");
  }
  
  // Reply with the JSON string
  json = cJSON_Print(root);
  mg_printf_data(conn, json);

  // Cleanup
  cJSON_Delete(root);
  free(json);
}

// Ping the ADC over the serial connection
static void
ping_adc(struct mg_connection *conn)
{
  int return_code = send_data_to_adc(SERIAL_COMMAND_PING, NULL, 0);

  if (return_code == SERIAL_RETURN_OKAY)
    mg_printf_data(conn, "{ \"success\": true }");
  else if (return_code == SERIAL_RETURN_TIMEOUT)
    mg_printf_data(conn, "{ \"success\": false, \"reason\": \"Timeout\" }");
  else
    mg_printf_data(conn, "{ \"success\": false, \"reason\": \"Error code returned from ADC: %d\" }", return_code);
}

// Update the ADC configuration
static void
update_adc(struct mg_connection *conn)
{
  int return_code;
  char input_signal_pin[10],
       output_signal_pin[10],
       input_led_pin[10],
       output_led_pin[10],
       n[10],
       arc_offset[10],
       arc_length[10],
       data[100];
  cJSON *root;
  char *json;

  // Retrieve and update variables
  if (mg_get_var(conn, "input_signal_pin", input_signal_pin, sizeof(input_signal_pin)) > 0)
    adc_config.input_signal_pin = atoi(input_signal_pin);

  if (mg_get_var(conn, "output_signal_pin", output_signal_pin, sizeof(output_signal_pin)) > 0)
    adc_config.output_signal_pin = atoi(output_signal_pin);

  if (mg_get_var(conn, "input_led_pin", input_led_pin, sizeof(input_led_pin)) > 0)
    adc_config.input_led_pin = atoi(input_led_pin);

  if (mg_get_var(conn, "output_led_pin", output_led_pin, sizeof(output_led_pin)) > 0)
    adc_config.output_led_pin = atoi(output_led_pin);

  if (mg_get_var(conn, "n", n, sizeof(n)) > 0)
    adc_config.n = atoi(n);

  if (mg_get_var(conn, "arc_offset", arc_offset, sizeof(arc_offset)) > 0)
    adc_config.arc_offset = atoi(arc_offset);
  
  if (mg_get_var(conn, "arc_length", arc_length, sizeof(arc_length)) > 0)
    adc_config.arc_length = atoi(arc_length);

  // Copy the configuration struct into a char array
  memcpy(data, &adc_config, sizeof(ADCConfiguration));

  // Send to the ADC
  return_code = send_data_to_adc(SERIAL_COMMAND_UPDATE, data, sizeof(ADCConfiguration));

  // Build the reply JSON string
  root = cJSON_CreateObject();
  cJSON_AddNumberToObject(root, "input_signal_pin", adc_config.input_signal_pin);
  cJSON_AddNumberToObject(root, "output_signal_pin", adc_config.output_signal_pin);
  cJSON_AddNumberToObject(root, "input_led_pin", adc_config.input_led_pin);
  cJSON_AddNumberToObject(root, "output_led_pin", adc_config.output_led_pin);
  cJSON_AddNumberToObject(root, "n", adc_config.n);
  cJSON_AddNumberToObject(root, "arc_offset", adc_config.arc_offset);
  cJSON_AddNumberToObject(root, "arc_length", adc_config.arc_length);

  if (return_code == SERIAL_RETURN_OKAY)
  {
    cJSON_AddTrueToObject(root, "success");
  }
  else
  {
    cJSON_AddFalseToObject(root, "success");
    cJSON_AddStringToObject(root, "reason", "Error code returned from ADC.");
    cJSON_AddNumberToObject(root, "error_code", return_code);
  }

  // Reply with the JSON string
  json = cJSON_Print(root);
  mg_printf_data(conn, json);

  // Cleanup
  cJSON_Delete(root);
  free(json);
}

static int
ev_handler(struct mg_connection *conn, enum mg_event ev)
{
  unsigned int rpm;

  switch (ev)
  {
    case MG_AUTH:
      return MG_TRUE;
      break;
    case MG_REQUEST:
      printf("Request received: %s\n", conn->uri);

      // Check if this is an api call
      if (strlen(conn->uri) >= 5 && strncmp(conn->uri, "/api/", 5) == 0)
      {
        // Check for /api/rpm
        if (strlen(conn->uri + 5) == 3 && strncmp(conn->uri + 5, "rpm", 3) == 0)
        {
          rpm =  60000000U / (get_pulse_period(100) * adc_config.n);
          printf("\tReturning RPM: %u\n", rpm);
          mg_printf_data(conn, "{ \"rpm\": %u, \"success\": true }", rpm);
          return MG_TRUE;
        }

        // Check for /api/connect
        if (strlen(conn->uri + 5) == 7 && strncmp(conn->uri + 5, "connect", 7) == 0)
        {
          connect_adc(conn);
          return MG_TRUE;
        }

        // Check for /api/adc
        if (strlen(conn->uri + 5) == 3 && strncmp(conn->uri + 5, "adc", 3) == 0)
        {
          update_adc(conn);
          return MG_TRUE;
        }

        // Check for /api/adc/ping
        if (strlen(conn->uri + 5) == 8 && strncmp(conn->uri + 5, "adc/ping", 8) == 0)
        {
          ping_adc(conn);
          return MG_TRUE;
        }
      }

      // Serve from filesystem
      return MG_FALSE;
      break;
    default:
      break;   
  }
  return MG_FALSE;
}


// ----------------------------------------------------------------
// Main Function
int
main(int argc, char** argv)
{
  // Check arguments
  if (argc == 2)
  {
    if (ini_parse(argv[1], on_ini_handler, NULL) < 0)
    {
      printf("Error: Can't load configuration file '%s'\n", argv[1]);
      return -1;
    }
  }
  else
  {
    printf("Usage: %s [configuration file]\n", argv[0]);
    return 0;
  }

  // Validate server config
  char absolute_path[100];
  if (realpath(server_config.document_root, absolute_path))
  {
    free(server_config.document_root);
    server_config.document_root = strdup(absolute_path);
  }
  else
  {
    printf("*** WARNING ***  Document root %s does not exist.\n", server_config.document_root);
  }

  // Initialise wiringPi
  wiringPiSetup();

  // Setup serial connection and ping the ADC
  setup_serial();
  //ping_adc();
  
  // Setup the input signal
  printf("Input signal receiving on GPIO.%u\n", server_config.gpio_input_signal_pin);
  pinMode(server_config.gpio_input_signal_pin, INPUT);
  pullUpDnControl(server_config.gpio_input_signal_pin, PUD_UP);
  
  // Setup the output signal
  printf("Output signal receiving on GPIO.%u\n", server_config.gpio_output_signal_pin);
  pinMode(server_config.gpio_input_signal_pin, INPUT);
  pullUpDnControl(server_config.gpio_input_signal_pin, PUD_UP);

  // Setup the web server
  struct mg_server *server = mg_create_server(NULL, ev_handler);
  mg_set_option(server, "listening_port", server_config.listening_port);
  mg_set_option(server, "document_root", server_config.document_root);

  printf("Document root located at %s\n", mg_get_option(server, "document_root"));
  printf("Listening on port %s\n", mg_get_option(server, "listening_port"));

  for (;;) {
    mg_poll_server(server, 1000);
  }

  mg_destroy_server(&server);

  return 0;
}