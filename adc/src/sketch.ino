#define LED_PIN                 13  // On board LED

// Structure to store ADC configuration data
struct ADCConfiguration
{
  // Input/Output signal and LED pins
  int16_t input_signal_pin;
  int16_t output_signal_pin;
  int16_t input_led_pin;
  int16_t output_led_pin;

  // Arc offset and length are stored as a multiple of tenths of
  // a degree. For example, a value of 123 is 12.3 degrees.

  // Number of degrees to offset the input signal by
  int16_t arc_offset;

  // Number of degrees to to lengthen the input signal
  int16_t arc_length;

  // Number of detector slugs/driver magnets/coils in the system
  int16_t n;
} adc_config;

// Adjusted period for a full rotation (pulse period * no. of detectors)
unsigned long rotational_period = 0;

// The last value of the input signal (HIGH or LOW)
int last_input_read = 0;

// The timestamp for the last detected falling edge of the input signal
// (in microseconds)
unsigned long last_input_falling_timestamp = 0;

// Time difference between consecutive falling edges of the input signal
// (in microseconds)
unsigned long pulse_period = 0;

// Width of the "on" (LOW) part of the input signal (in microseconds)
unsigned long pulse_width = 0;

// The timestamp of the last crossover point (in microseconds)
unsigned long crossover_point_time = 0;

// Calculated signal timings for the output signal
unsigned long current_arc_start = 0;
unsigned long next_arc_start = 0;
unsigned long arc_period = 0;

// Serial command codes
enum SerialCommandCodes
{
  SERIAL_COMMAND_UPDATE = 1,
  SERIAL_COMMAND_PING = 2
};

// Serial return codes
enum SerialReturnCodes
{
  SERIAL_RETURN_TIMEOUT = -1,
  SERIAL_RETURN_OKAY = 1,
  SERIAL_RETURN_ERROR_UNKNOWN_COMMAND = 2,
  SERIAL_RETURN_ERROR_INVALID_DATA = 3,
  SERIAL_RETURN_ERROR = 4
};

// -------------------------------------------------------------
// Input Signal Handler
void input_signal_handler()
{
  unsigned long now = micros();
  int input_read = digitalRead(adc_config.input_signal_pin);
  
  if (input_read == HIGH && last_input_read == LOW)  // low -> high = rising edge
  {
    // rising edge
    // record the pulse width
    pulse_width = time_diff(last_input_falling_timestamp, now);
  }
  else if (input_read == LOW && last_input_read == HIGH)  // high -> low = falling edge
  {
    // falling edge
    // record the falling edge period
    pulse_period = time_diff(last_input_falling_timestamp, now);
    
    // recalculate the rotational velocity
    rotational_period = pulse_period * adc_config.n;
    
    // track the time of each falling edge
    last_input_falling_timestamp = now;
  }
  
  digitalWrite(adc_config.input_led_pin, !input_read);
  last_input_read = input_read;
}

// -----------------------------------------------------------------
// Ouput Singal Handler
void output_signal_handler()
{
  unsigned long now = micros();
    
  if (rotational_period && rotational_period < 600000UL)  // ~ 100 RPM
  {
    current_arc_start = (adc_config.arc_offset >= 0) ?
      last_input_falling_timestamp + adc_config.arc_offset * rotational_period / 3600UL :
      last_input_falling_timestamp - (unsigned long)(-adc_config.arc_offset) * rotational_period / 3600UL;
    next_arc_start = current_arc_start + pulse_period;
    arc_period = adc_config.arc_length * rotational_period / 3600UL;
    
    if (is_in_arc(now, current_arc_start, current_arc_start + arc_period)
      || is_in_arc(now, next_arc_start, next_arc_start + arc_period))
    {
      // Drive the output signal pin low
      digitalWrite(adc_config.output_signal_pin, LOW);
      digitalWrite(adc_config.output_led_pin, HIGH);
    }
    else
    {
      // Float the output signal pin
      digitalWrite(adc_config.output_signal_pin, HIGH);
      digitalWrite(adc_config.output_led_pin, LOW);
    }
  }
  else
  {
    // Just passthrough the input signal
    digitalWrite(adc_config.output_signal_pin, last_input_read);
    digitalWrite(adc_config.output_led_pin, !last_input_read);
  }
}

void setup_pins()
{
  // setup the input signal
  pinMode(adc_config.input_signal_pin, INPUT_PULLUP);
  last_input_read = HIGH;
  
  // setup the output signal, leave floating
  pinMode(adc_config.output_signal_pin, OUTPUT);
  
  // set up the status LEDs
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  pinMode(adc_config.input_led_pin, OUTPUT);
  digitalWrite(adc_config.input_led_pin, HIGH);
  pinMode(adc_config.output_led_pin, OUTPUT);
  digitalWrite(adc_config.output_led_pin, HIGH);
}

void setup()
{
  // Setup serial connection
  Serial.begin(9600);
  
  // Sane default config
  adc_config.input_signal_pin = 22;
  adc_config.output_signal_pin = 23;
  adc_config.input_led_pin = 52;
  adc_config.output_led_pin = 53;
  adc_config.arc_offset = -60;
  adc_config.arc_length = 205;
  adc_config.n = 8;

  // Setup I/O pins
  setup_pins();
}

void loop()
{
  input_signal_handler();
  output_signal_handler();
}

// Handle serial commands coming from the controller
void serialEvent()
{
  // Indicate serial connection in use
  digitalWrite(LED_PIN, HIGH);

  char packet[100], data[100];
  char code, length, num, val;

  // Get the code
  Serial.readBytes(&code, 1);
  Serial.readBytes(&length, 1);
  switch(code)
  {
    case SERIAL_COMMAND_UPDATE:
      if (length == sizeof(ADCConfiguration))
      {
        num = Serial.readBytes(data, sizeof(ADCConfiguration));
        if (num == sizeof(ADCConfiguration))
        {
          // Copy the data into the config struct
          memcpy(&adc_config, data, sizeof(ADCConfiguration));

          // Setup I/O pins
          setup_pins();

          // Return success code
          Serial.write(SERIAL_RETURN_OKAY);
        }
        else
        {
          // Data is incorrect size
          Serial.write(SERIAL_RETURN_ERROR_INVALID_DATA);
        }
      }
      else
      {
        // Data is incorrect size
        Serial.write(SERIAL_RETURN_ERROR_INVALID_DATA);
      }
      break;
    case SERIAL_COMMAND_PING:
      // Return success code
      Serial.write(SERIAL_RETURN_OKAY);
      break;
    default:
      Serial.write(SERIAL_RETURN_ERROR_UNKNOWN_COMMAND);
      break;
  }

  Serial.flush();
  //delay(100);

  // Indicate finished using serial connection
  digitalWrite(LED_PIN, LOW);
}

// Checks if the the target time is inside an arc, true = yes, false = no
int is_in_arc(unsigned long target, unsigned long arc_start, unsigned long arc_end)
{
  return ((arc_start <= arc_end && (target >= arc_start && target <= arc_end))
    || ((arc_start > arc_end) && (target >= arc_start || target <= arc_end)));
}

// Calculate the difference between two times, taking into account overflow
// Assumes that time1 is older or equal to time2, and that time2 has not
// "elapsed" time1
unsigned long time_diff(unsigned long time1, unsigned long time2)
{
    if (time1 > time2)
        return 0xFFFFFFFFUL - time1 + time2;
    else return time2 - time1;
}

// Verifies the config data has been received correctly and, if so,
// updates the global config variable
// Data packet should consist of the config struct followed by the
// bitwise inverse of itself
int verify_config_data(const char* data)
{
  int i;
  
  // Verify the data
  for (i = 0; i < sizeof(ADCConfiguration); ++i)
  {
    if (data[i] ^ data[i + sizeof(ADCConfiguration)] != 255)
      return false;
  }
  
  // Update the global config struct
  memcpy(&adc_config, data, sizeof(ADCConfiguration)); // copy byte array to struct
  return true;
}
